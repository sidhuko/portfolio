import fs from 'fs'
import path from 'path'
import util from 'util'
import yaml from 'js-yaml'
import fm from 'front-matter'
import md from 'markdown-it'
import hljs from 'highlight.js'

const exists = util.promisify(fs.exists)
const readDir = util.promisify(fs.readdir)
const readFile = util.promisify(fs.readFile)

function getMarkdown(file, ext, contents) {
  const { attributes, body } = fm(contents)
  return {
    name: path.basename(file, ext),
    excerpt: body.match(/(.*\s)/, '')[1],
    content: md({
      html: true,
      linkify: true,
      typographer: true,
      highlight: (str, lang) => {
        if (lang && hljs.getLanguage(lang)) {
          try {
            return hljs.highlight(lang, str).value;
          } catch (__) {
            return ''
          }
        }
        return ''
      },
    }).render(body.replace(/(.*)---/, '')),
    data: attributes,
    slug: attributes.title
      .toLowerCase()
      .replace(/ /g, '-')
      .replace(/[^\w-]+/g, '')
  }
}

export default async function parser(directory) {
  if (!await exists(directory)) throw Error('Directory does not exist')

  const files = (await readDir(directory)).map(async file => {
    const ext = path.extname(file)
    const contents = await readFile(path.resolve(directory, file), 'utf8')

    switch (ext) {
      case '.md':
        return getMarkdown(file, ext, contents)
      case '.yaml':
      case '.yml':
        return {
          name: path.basename(file, ext),
          data: yaml.safeLoad(contents)
        }
      default:
        return contents
    }
  })

  return Promise.all(files)
}
