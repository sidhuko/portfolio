import React from 'react'
import { Router } from 'react-static'
import Routes from 'react-static-routes'

import './css/app.css'
import Video from './components/Video'
import Navbar from './components/Navbar'

const App = () => (
  <Router>
    <div className="app">
      <Navbar />
      <Routes />
      <div className="container article">
        <hr />
        <hr />
        <Video src="//www.youtube.com/embed/I1YM476Pa4o" image="/uploads/schism.jpg" />
        <blockquote className="app-blockquote">
          <h2>"Finding beauty in the dissonance."</h2>
          <p><small className="lead">Maynard James Keenan</small></p>
        </blockquote>
        <hr />
      </div>
      <div className="footer section">
        <div className="container">
          <small>&copy; James Sidhu 2018</small>
        </div>
      </div>
    </div>
  </Router>
)

export default App
