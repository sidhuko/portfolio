const React = require('react')

export default ({ Html, Head, Body, children, renderMeta }) => (
  <Html>
    <Head>
      <meta charSet="utf-8" />
      <meta name="theme-color" content="#20232A"/>
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
      <meta name="description" content="With several years of experience working within startups, Greenfield projects and corporate environments, specialising in fullstack development, I collaborate across product, testing, development and infrastructure teams to deliver business-focused applications within the Agile software methodologies." />
      <title>James Sidhu</title>
      <link rel="manifest" href="/manifest.json" />
      <link rel="shortcut icon" href="/favicon.ico" />
      <link rel="icon" type="image/png" href="/favicon.png" />
      <link rel="preload" as="font" href="https://fonts.gstatic.com/s/raleway/v12/1Ptrg8zYS_SKggPNwJYtWqZPANqczVs.woff2" type="font/woff2" crossOrigin="true" />
      <link rel="preload" as="font" href="https://fonts.gstatic.com/s/montserrat/v12/JTUSjIg1_i6t8kCHKm459WlhyyTh89Y.woff2" type="font/woff2" crossOrigin="true" />
      <link rel="preload" as="font" href="https://fonts.gstatic.com/s/montserrat/v12/JTURjIg1_i6t8kCHKm45_dJE3gnD_vx3rCs.woff2" type="font/woff2" crossOrigin="true" />
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700|Raleway:700" rel="stylesheet" />
      { renderMeta.styleTags }
    </Head>
    <Body>
      { children }
    </Body>
  </Html>
)
