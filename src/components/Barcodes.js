import React from 'react'

class Barcodes extends React.Component {

  static getRandomBool() {
    return Math.random() >= 0.5
  }

  static getInt(min, max) {
    return Math.random() * (max - min) + min
  }

  static getRandomNumber(min, max) {
    return Math.floor(Math.random() * (max - Barcodes.getInt(min, max) + 1)) + min
  }

  static onBarcodeFocus(e) {
    e.target.classList.add('barcode-animate')
  }

  static onBarcodeAnimationEnd(e) {
    e.target.classList.remove('barcode-animate')
    e.target.classList.remove('barcode-animate-inverted')
  }


  constructor(props) {
    super(props)
    this.timers = []
    this.reset = false
    this.state = {
      columnWidth: 20,
      screenWidth: (typeof window !== 'undefined') ? window.innerWidth : 1080,
    }
  }

  addInterval(fn, ms) {
    this.timers.push(setInterval(fn, ms))
  }

  resize() {
    if(this.reset) {
      clearTimeout(this.reset)
    }

    this.reset = setTimeout(() => {
      this.setState({ screenWidth: window.innerWidth })
      this.reset = false
    }, 250)
  }

  componentDidMount() {
    if (typeof window !== 'undefined') {
      window.addEventListener("resize", this.resize.bind(this))
    }

    if (this.timers.length === 0) {
      this.addInterval(() => {
        const barcodes = document.getElementsByClassName('barcode')
        barcodes[Barcodes.getRandomNumber(1, barcodes.length)].classList.add(
          Barcodes.getRandomBool() ? 'barcode-animate' : 'barcode-animate-inverted'
        )
      }, 2200)

      this.addInterval(() => {
        const barcodes = document.getElementsByClassName('barcode')
        barcodes[Barcodes.getRandomNumber(1, barcodes.length)].classList.add(
          Barcodes.getRandomBool() ? 'barcode-animate' : 'barcode-animate-inverted'
        )
      }, 1500)
    }

  }

  componentWillUnmount() {
    if (typeof window !== 'undefined') {
      window.removeEventListener("resize", this.resize.bind(this))
    }

    this.timers.forEach((i) => clearInterval(i))
    this.timers = []
  }

  columns() {
    return Array.from({ length: Math.ceil(this.state.screenWidth / this.state.columnWidth) }, (v, i) => (
      <span
        key={i}
        className="barcode"
        onMouseEnter={Barcodes.onBarcodeFocus}
        onAnimationEnd={Barcodes.onBarcodeAnimationEnd}
        style={ {
          width: `${Barcodes.getRandomNumber(10, 50)}px`,
          paddingLeft: `${Barcodes.getRandomNumber(2, 10)}px`,
          paddingRight: `${Barcodes.getRandomNumber(2, 10)}px`,
        } } />
    ))
  }

  render() {
    const { image, color } = this.props
    return (
      <div className="barcodes">
        { this.columns() }
        {(() => {
          if (image) {
            return (
              <div className="barcodes-image" style={ {
                background: `url(${image}) no-repeat center center`,
                filter: (color) ? 'blur(5px) brightness(70%)' : 'grayscale(100%) blur(5px) brightness(80%)',
              } } />
            )
          }
        })()}
      </div>
    )
  }

}

export default Barcodes
