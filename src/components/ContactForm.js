import React from 'react'

const webhook = 'https://hooks.slack.com/services/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'

class ContactForm extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isSuccessful: false,
      isFatalError: false,
      isCallbackRequested: false,
    }
    this.name = React.createRef()
    this.email = React.createRef()
    this.phone = React.createRef()
    this.message = React.createRef()
    this.send = this.send.bind(this)
    this.toggleCallback = this.toggleCallback.bind(this)
  }

  toggleCallback() {
    this.setState({ isCallbackRequested: !this.state.isCallbackRequested })
  }

  async send(e) {
    e.preventDefault();
    try {
      const name = this.name.current.value
      const email = this.email.current.value
      const phone = (this.phone.current !== null) ? this.phone.current.value : false
      const message = this.message.current.value

      let text = `*${name.split(' ')[0]} sent a message from jamessidhu.com* \n${message} \n_*Name:* ${name}_ \n_*Email:* ${email}_ \n`

      if (phone) {
        text += `_*Phone:* ${phone}_ \n`
      }

      await fetch(webhook, {
        method: 'POST',
        mode: 'no-cors',
        cache: 'no-cache',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({ text })
      })
      this.setState({ isSuccessful: true })
    } catch (err) {
      console.error(err)
      this.setState({ isFatalError: true })
    }

  }

  render() {
    const { isFatalError, isSuccessful, isCallbackRequested } = this.state

    if (isFatalError) {
      return (
        <p className="subtitle">An unexpected error has occured. Please send your request to james.thomas.sidhu@gmail.com</p>
      )
    }

    if (isSuccessful) {
      return (
        <p className="subtitle">Thanks! I'll get back to you shortly.</p>
      )
    }

    return (
      <form onSubmit={this.send}>
        <label htmlFor="name">
          Name <input type="text" id="name" required ref={this.name} />
        </label>

        <label htmlFor="email">
          Email <input type="email" id="email" required ref={this.email} />
        </label>

        {(() => {
          if (isCallbackRequested) {
            return (
              <label htmlFor="phone">
                Phone
                <input type="tel" id="phone" required ref={this.phone} minLength="9" maxLength="14" />
              </label>
            )
          }
        })()}

        <label htmlFor="message">
          Message
          <textarea id="message" required ref={this.message} />
        </label>

        <p>
          <label className="label-inline" htmlFor="confirmField">
            Request a callback? &nbsp;
          <input type="checkbox" id="confirmField" onChange={ () => this.toggleCallback() } />
          </label>
        </p>

        <button type="submit" className="button-primary float-right">Submit</button>
        <hr />

      </form>
    )
  }

}

export default ContactForm
