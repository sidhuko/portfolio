import React from 'react'
import { Link } from 'react-static'


class Navbar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      isOpen: false,
    }
    this.clear = this.clear.bind(this)
    this.toggle = this.toggle.bind(this)
  }

  toggle() {
    this.setState({ isOpen: !this.state.isOpen })
  }

  clear() {
    this.setState({ isOpen: false })
  }

  render() {

    let classes = "navbar-dropdown";

    if (this.state.isOpen) {
      classes += " navbar-dropdown-open"
    }

    return (
      <nav className="navbar" role="navigation" aria-label="main navigation">
        <div className="container">

          <div className="navbar-brand">
            <Link className="navbar-logo" exact to="/" href="/" onClick={this.clear}>
                James Sidhu
            </Link>
          </div>

          <div className="navbar-menu">
            <button type="button" className="navbar-item navbar-hamburger" href="#" onClick={this.toggle}>
              &#9776;
            </button>
            <div className={classes}>
              <Link className="navbar-item" exact to="/" href="/" onClick={this.clear}>
                About
              </Link>
              <Link className="navbar-item" to="/cv" href="/cv" onClick={this.clear}>
                CV
              </Link>
              {/* <Link className="navbar-item" to="/blog" href="/blog" onClick={this.clear}>
                Blog
              </Link> */}
              <Link className="navbar-item" to="/contact" href="/contact" onClick={this.clear}>
                Contact
              </Link>
            </div>
          </div>

        </div>
      </nav>
    )
  }

}

export default Navbar
