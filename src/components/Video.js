import React from 'react'

class Video extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      loaded: false,
    }
    this.load = this.load.bind(this)
  }

  load(e) {
    e.preventDefault()
    this.setState({ loaded: true })
  }

  render() {
    if (this.state.loaded) {
      return (
        <iframe className="app-frame" title="Tool Schism" src={ `${this.props.src}?rel=0&amp;showinfo=0&amp;autoplay=1` } frameBorder="0" allow="autoplay; encrypted-media" allowFullScreen />
      )
    }

    return (
      <button type="button" className="app-frame" onClick={this.load}>
        <img src={this.props.image} alt={this.props.src} style={{ maxWidth: '100%' }}  />
      </button>
    )
  }

}

export default Video
