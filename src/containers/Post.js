import React from 'react'
import { withRouteData, Link } from 'react-static'
import Moment from 'react-moment'
import Markdown from 'react-markdown'

import Barcodes from '../components/Barcodes'

export default withRouteData(({ post }) => (
  <div className="section">
    <div className="container hero article">
      <small><Moment format="MMMM Do, YYYY">{post.data.date}</Moment> by <Link to="/cv" href="/cv">James Sidhu</Link></small>
      <h1 className="title">{post.data.title}</h1>
      <Link to="/blog/" href="/blog" className="button">Back to articles</Link>
    </div>
    <hr />
    <div className="container article article-white">
      <div className="article-content">
        <p className="lead">{post.excerpt}</p>
        <Markdown source={post.content} escapeHtml={false} />
        <hr />
      </div>

    </div>

    <Barcodes image={post.data.thumbnail} color />
  </div>
))
