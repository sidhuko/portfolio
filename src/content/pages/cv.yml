title: Cirriculam Vitae
image: /uploads/norfolk.jpg
subtitle: >-
  An energetic and self-taught computer enthusiast. I was assembling computers and learning programming as a teenager developing my skills towards full stack development. Gaining experience in networking, virtualisation, server deployments and web application security whilst working with high-profile clients.
current:
  position: Senior Fullstack Developer
  company: Concrete Media
  url: www.concreteplatform.com
  startDate: Mar 2018
  endDate: present
experiences:
  - position: Javascript Developer
    company: Cambridge University Press
    url: cambridge.org
    startDate: May 2017
    endDate: Feb 2018
    duration: 10 mos
    opportunities:
      - Prototyping of containerising an existing node.js platform with Gitlab, Tectonic and Kubernetes.
      - Existing monolithic application reaching memory limits of Node.js so implementing safer process management and pruning dependency tree.
      - BAU tasks mitigating new attack vectors, diagnosing production environment issues and performance issues by introducing new tooling such as pm2.
      - Integrating an OAuth2 single sign-on solution with Keycloak for multiple realms from an existing identity database using JWT tokens.
  - position: Senior Fullstack Developer
    company: Spotlight Data
    url: spotlightdata.co.uk
    startDate: Oct 2015
    endDate: Feb 2017
    duration: 1 yr 5 mos
    opportunities:
      - Architect a PoC solution to provide async text extraction, storage and analysis using containers and message queues from privately stored Amazon S3 files.
      - Build Docker containers for development and production which used custom libraries to provide OCR and other text extraction within Node.js providing a test framework for local development.
      - Production application was roadmap, planned and designed to alpha release working with stakeholders across the business into Aha!, Jira and Confluence to gain time estimates and our release schedule.
      - Immutable data solution provided using PostgraphQL and PGSQL to provide custom models and database constraints based on a JSON Schema definition library.
  - position: Node.js Developer
    company: Adstream
    url: www.adstream.com
    startDate: Aug 2015
    endDate: Nov 2014
    duration: 10 mos
    opportunities:
      - Implement an Express RESTful API for paid users based working toward international standards such as JSON Schema and HyperSchema using an underlying core system and data structure.
      - A large proportion of my work was focused on infrastructure. Without an environment to vertically scale available I developed systems using node domain, clustering and varying levels of error handling to keep the process alive unless it was absolutely necessary to restart the worker. I was also responsible supporting the devops team by debugging problems in infrastructure and other projects codebases.
      - Most of the project was written in Ramda, Promises and Highland to maintain a functional reactive program paradigm with a clear separation of concerns so the project had clear and concise code for full time employees to easily understand.
      - I was responsible for the agile sprints and maintaining tickets. Ensuring the work was collated into components and correctly specified by business and the Chief Technical Architect. This involved liaising with all sections of the business such as clients, account managers and IT support.
  - position: JavaScript Developer
    company: Scorem
    url: false
    startDate: Mar 2014
    endDate: Oct 2014
    duration: 8 mos
    opportunities:
      - Produce hybrid applications for Android (3+) and iOS (6+) based on existing website within an Angular architecture using Cordova and NPM tools. Responsible for administrating iOS Dev Centre account for devices, provision profiles and certificates.
      - Implementing OAuth system, HTML5 canvas image cropper using native Javascript features, such as FileReader, to minimise external XHR requests as well as providing offline functionality for intermittent connections using ngCordova.
      - Using native features where possible, contacts and share functionality, to reproduce the websites functionality within a native app.
      - Creating a Node.js server for proxying API requests which required authentication via OAuth and token based. This was using q-io library, graphicsmagick and Google Custom Search for website feartures such as a Google Images search and editing images.
  - position: JavaScript Developer
    company: Sky
    url: sky.com
    startDate: Jan 2014
    endDate: Apr 2014
    duration: 4 mos
    opportunities:
      - Investigate performance issues for cricket live score iOS and Android in AngularJS whilst providing guidance and a performance budget for a multisport live score centre. The new application will be used as a responsive website as well as a hybrid app for iOS and Android device.
      - Bespoke architecture within Angular to minimise data maintained within memory for smaller mobile devices (iPod) whilst providing alternative displays to larger and more powerful devices. Creating new system to merge API requests into one XHR request as well as providing a clear upgrade path to implement websockets at a later time.
      - Achieved a large reduction of codebase to 5% of original LoC and a clear path for SEO prerender to provide search engines with content. Working in agile environment with Jira, Confluence and Gitlab.
  - position: Frontend Developer
    company: Vice Media
    url: vice.com
    startDate: Sep 2013
    endDate: Dec 2013
    duration: 4 mos
    opportunities:
      - Responsive website design using mobile first techniques specifically targeting iOS devices (iPhone/iPad) with optional retina support.
      - Producing Object Oriented Javascript within Vanilla and AngularJS javascript environments using the Zepto library where practical. Templates written using semantic HTML5 and CSS3 templates.
      - Interviewing Senior Frontend Developers and technically assessing abilities in HTML5, CSS3 and Object Oriented Javascript (AngularJS) for vacancies.
      - Implement frontend build process using Node.js and Grunt. Specifically using Stylus, html2js and Require.JS to minimise XHR requests and increase perceived loading time for mobile users.
  - position: Web Developer
    company: Williams Lea
    url: www.wlt.com
    startDate: Feb 2013
    endDate: Aug 2013
    duration: 7 mos
    opportunities:
      - Building progressively enhanced and W3C/WCAG compliant components for a large government project with SVN, LESS, jQuery, CSS3 & HTML5.
      - Producing modular jQuery plugins with documentation and QUnit tests using Node and Grunt & Express.
      - Configuring Apache and Tomcat servers for SSL, session replication and sticky sessions. Also using reverse proxies and mod_rewrite for custom redirections to other services.
      - Implementing a frontend build script for all environments (several servers, testing and local development) using Maven and Ant. Producing clear guidance on installation and usage within the documentation for Frontend, XML, Java and Semantic developers.
  - position: Web Developer
    company: Knit
    url: www.foolproof.co.uk
    startDate: Nov 2012
    endDate: Feb 2013
    duration: 5 mos
    opportunities:
      - Working for high profile multinational clients in a small start-up agency which produced high quality frontend and application development for event based marketing.
      - Producing responsive design for LG's Magic is in your hand tour specifically for the use on iPhones and iPads for representatives at The Gadget Show Live.
      - Building secure competition systems and providing interactive statistics from MySQL databases which were running interactive Facebook applications we produced for competitions.
testimonials:
  - name: Lee Powell
    position: Javascript Software Architect at Aviva
    description: >-
      James is an exceptional developer with the utmost attention to detail. His interest in bleeding edge techniques always sees him pushing boundaries and best practises on what it actually possible. Coupled with the ability to write high quality, testable code makes him an asset to any team.
      He has a wide range of knowledge from server to the client side in various languages (as well as system admin), and has recently developed a strong interest in Node and Angular - picking them both up very quickly to a very high ability.
      I would happily work alongside James again - and hope to do so in the future.
  - name: Travis Lee Street
    position: Designer, Developer, Consultant & Teaching Fellow
    description: >-
      Jimmy is one of those creative geniuses that only come around once in a life-time. From SEO to web development, Jimi has consistently proven that he is a capable project manager and developer. His skills were integral in the creation and completion of over a dozen projects in the time he worked for Kiss and I would highly recommend his talents and abilities to anyone lucky enough to cross his path.
