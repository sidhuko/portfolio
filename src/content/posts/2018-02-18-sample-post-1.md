---
layout: blog
title: 'Adding a service worker to a React Static and Netlify CMS website'
date: 2018-02-19T00:45:43.212Z
thumbnail: /uploads/image2.jpg
rating: '5'
---
I've recently launched my new [portfolio](https://www.jamessidhu.com) using React Static with a blog powered by [Netlify CMS](https://netlifycms.org). One of the first performance improvements is adding a service worker to improve the time of rendering and performance between navigating the pages. You won't find a service worker included in React Static because of edge cases, which requires customisations for different types of application, but it's a simple task to do when you draw from the concepts from universal and progressive web applications.
---
The React Static library uses similar conventions to the Create React App and exposes a Webpack configuration object within the root `static.config.js` which is where will apply our service worker using the recommended [Progressive Web Application] (https://webpack.js.org/guides/progressive-web-application/) setup.

In this guide, you'll find out how to generate service workers with [workbox-webpack-plugin](https://developers.google.com/web/tools/workbox/modules/workbox-webpack-plugin). First of you need to install the plugin within your pack.

```
npm i workbox-webpack-plugin --save-dev
```
