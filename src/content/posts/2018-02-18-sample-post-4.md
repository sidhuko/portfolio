---
layout: blog
title: Using Skaffold and Lerna to boast your service based architecture delivery
date: 2018-02-19T00:55:55.386Z
thumbnail: /uploads/steel-3387882_1280.jpg
rating: '5'
---
Using an established monorepo management tool, Lerna, and the kubernetes incubator project Skaffold I will show you how to build, deploy and maintain services from a single configuration file to a local, preproduction or production deployment.
---
Nam ex quod doctus omnesque. Eos dico fastidii cu, nulla aliquip et eam, diam possim periculis sit eu. Sint mollis no quo, et eam posse postea mandamus. Te has altera fabulas, te eam democritum conclusionemque, ea nemore eirmod scripserit mea. Purto case homero per at. Nam eirmod delenit placerat ex, nam eu fabellas adversarium, fabulas forensibus eam cu. Per zril vocibus signiferumque ad, omnis equidem necessitatibus ad quo.

Cu vim nusquam efficiendi. Dicat recusabo suavitate per te, sea ei dicunt persius pertinax. Ea vel graecis euripidis, decore invenire in ius. Mea vide blandit reformidans ad, nam ne primis vocibus, qui dicta splendide appellantur cu. Duo ei dicunt suscipiantur. Quodsi accusam consequuntur at nam, te iisque elaboraret complectitur sea, tritani sensibus indoctum et eam.

Cum zril iisque detracto ex. Ex vivendum intellegat mea, nec ex essent pericula mediocrem, id sit munere putant. Sea et congue numquam erroribus, labitur facilisis eos ut. Est sint percipit te, ex sumo wisi temporibus usu. Erroribus elaboraret his ad, ad est veniam insolens vulputate. Qui ne eripuit nominavi epicurei, alterum accommodare sea at, est te omnes elaboraret.

Nam ut brute sonet habemus. Inani meliore luptatum an his, deseruisse incorrupte repudiandae mel in. Movet constituam cum ex, clita scripserit te cum, his dicat verear sententiae in. Eu quo sumo dictas. No est probo justo.
