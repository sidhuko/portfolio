import 'whatwg-fetch'
import React from 'react'
import ReactGA from 'react-ga'
import ReactDOM from 'react-dom'
import { AppContainer } from 'react-hot-loader'
import hljs from 'highlight.js'

import App from './App'

if (typeof document !== 'undefined') {

  ReactGA.initialize('UA-124655166-1');
  ReactGA.pageview(window.location.pathname + window.location.search);

  if (navigator && 'serviceWorker' in navigator) {
    window.addEventListener('load', () => {
      navigator.serviceWorker.register('/service-worker.js').then(registration => {
        console.log('SW registered: ', registration);
      }).catch(registrationError => {
        console.log('SW registration failed: ', registrationError);
      });
    });
  }

  const renderMethod = module.hot
    ? ReactDOM.render
    : ReactDOM.hydrate || ReactDOM.render
  const render = Comp => {
    renderMethod(
      <AppContainer>
        <Comp />
      </AppContainer>,
      document.getElementById('root')
    )
  }

  render(App)

  hljs.initHighlightingOnLoad()

  if (module.hot) {
    module.hot.accept('./App', () => render(require('./App').default))
  }
}

export default App
