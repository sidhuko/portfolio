import React from 'react'
import { Link } from 'react-static'
import Barcodes from '../components/Barcodes'

export default () => (
  <div className="section">
    <div className="container hero article">
      <Link to="/" href="/">
        <h1 className="title"><span>Sorry, that page doesn't exist</span></h1>
        <p className="subtitle">Return to the homepage</p>
      </Link>
      <hr />
    </div>
    <Barcodes image="/uploads/norfolk.jpg" color />
  </div>
)
