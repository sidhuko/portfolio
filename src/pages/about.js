import React from 'react'
import { withRouteData, Link } from 'react-static'
import Barcodes from '../components/Barcodes'

export default withRouteData(({ about }) => (
  <div className="section">
    <div className="container hero article">
      <Link to="/cv" href="/cv">
        <h1 className="title"><span dangerouslySetInnerHTML={{ __html: about.title}} /></h1>
        <p className="subtitle">{ about.subtitle }</p>
      </Link>
      <Link className="button" to="/cv" href="/cv">View CV</Link>
      <hr />
    </div>

    <div className="container">
      <h2 className="title-section">Key skills</h2>
    </div>

    <div className="container article article-white">
        { about.skills.map((skill, i) => (
          <div key={`skill-${i}`} className="article-50">
            <h3><span aria-label={ skill.name } role="img">{ skill.icon } &nbsp; </span> { skill.name }</h3>
            <p>{ skill.description }</p>
            <hr />
          </div>
        ))}
      <div className="clearfix" />
    </div>

    <Barcodes image={ about.image } color />
  </div>
))
