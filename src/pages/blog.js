import React from 'react'
import Moment from 'react-moment'
import { withRouteData, Link } from 'react-static'
import Barcodes from '../components/Barcodes'

export default withRouteData(({ posts }) => {
  const data = [...posts]
  const $latest = data.shift()
  return (
    <div className="section">
      <div className="container hero article">
        <Link to={`/blog/post/${$latest.slug}`} href={`/blog/post/${$latest.slug}`}>
          <small><Moment format="MMMM Do, YYYY">{$latest.data.date}</Moment></small>
          <h1 className="title"><span>{ $latest.data.title }</span></h1>
          <p className="subtitle">{ $latest.excerpt }</p>
        </Link>
        <hr />
      </div>

      <div className="container">
        <h2 className="title-section">Latest articles</h2>
      </div>
      <div className="container hero article">
        {data.map((post, i) => (
          <div key={i} className="article-50">
            <Link to={`/blog/post/${post.slug}`} href={`/blog/post/${post.slug}`}>
              <small><Moment format="MMMM Do, YYYY">{post.data.date}</Moment></small>
              <h3 className="title"><span>{ post.data.title }</span></h3>
              <p>{ post.excerpt }</p>
            </Link>
          </div>
        ))}

      </div>

      <Barcodes image={ $latest.data.thumbnail } />
    </div>
    )
})
