import React from 'react'
import { withRouteData } from 'react-static'
import Barcodes from '../components/Barcodes'
import ContactForm from '../components/ContactForm'

const connections = [
  {
    name: 'LinkedIn',
    url: 'https://www.linkedin.com/in/jamesthomassidhu/',
    icon: '//logo.clearbit.com/linkedin.com',
  },
  {
    name: 'Github',
    url: 'https://github.com/sidhuko',
    icon: '//logo.clearbit.com/github.com',
  },
  {
    name: 'Gitlab',
    url: 'https://gitlab.com/sidhuko',
    icon: '//logo.clearbit.com/gitlab.com',
  },
  {
    name: 'StackOverflow',
    url: 'https://stackoverflow.com/users/1181362/sidhuko',
    icon: '//logo.clearbit.com/stackoverflow.com',
  },
]

export default withRouteData(({ contact }) => (
  <div className="section">
    <div className="container hero article">
      <h1 className="title">
        { contact.title }
      </h1>
      <p className="subtitle">
        { contact.subtitle }
      </p>
    </div>

    <div className="container article">
        <div className="contact-form">
          <h2 className="title-section">Contact form</h2>
          <ContactForm />
          <div className="clearfix">&nbsp;</div>
        </div>
        <div className="contact-services">
          <h2 className="title-section">Make a connection</h2>
          <div className="services">
            {connections.map((service, i) => (
              <a key={i} target="_blank" rel="noopener noreferrer" href={service.url} className="subtitle" style={{ background: 'white', padding: '0', margin: '0' }}>
                <img src={`${service.icon}?size=80&grayscale`} alt={service.name} style={{ position: 'relative', margin: 0, padding: 0, top: '50%', transform: 'translateY(-50%)' }} />
              </a>
            ))}
            <div className="clearfix" />
          </div>
        </div>
    </div>

    <Barcodes image={ contact.image } color />
  </div>
))
