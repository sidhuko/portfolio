import React from 'react'
import { withRouteData, Link } from 'react-static'
import Barcodes from '../components/Barcodes'

export default withRouteData(({ cv }) => (
  <div className="section">
      <div className="container hero article">
        <h1 className="title">{ cv.title }</h1>
        <p className="subtitle" dangerouslySetInnerHTML={{ __html: cv.subtitle}} />
        <a className="button" target="_blank" rel="noopener noreferrer" href="https://www.linkedin.com/in/jamesthomassidhu/">Connect on LinkedIn</a> &nbsp;
        <Link className="button" to="/contact" href="/contact">Send a message</Link>
        <hr />
      </div>

      <div className="container">
        <h2 className="title-section" id="#experiences">Current position</h2>
      </div>

      <div className="container article">

          <div>
            <h4>
              { (cv.current.url) && (
                <span style={{ display: 'inline-block', float: 'right', background: 'white', width: '80px', height: '80px', borderRadius: '40px', overflow: 'hidden', border: '1px solid #fff' }}>
                  <img style={{ position: 'relative', margin: 0, padding: 0, top: '50%', transform: 'translateY(-50%)' }} src={ `//logo.clearbit.com/${cv.current.url}?size=80` } alt={ `${cv.current.company} logo`}/>
                </span>
              ) }

              <strong>{ cv.current.position }</strong> <br />{ cv.current.company }
            </h4>
            <small>{ cv.current.startDate } - { cv.current.endDate }</small>
            <hr />
          </div>
      </div>

      <div className="container">
        <h2 className="title-section" id="#experiences">Experiences</h2>
      </div>
      <div className="container article article-white">
          { cv.experiences.map((exp, i) => (
            <div key={ `experience-${i}` } className="article-50">
              <h4>
                { (exp.url) && (
                  <span style={{ display: 'inline-block', float: 'right', background: 'white', width: '80px', height: '80px', borderRadius: '40px', overflow: 'hidden', border: '1px solid #fff' }}>
                    <img style={{ position: 'relative', margin: 0, padding: 0, top: '50%', transform: 'translateY(-50%)' }} src={ `//logo.clearbit.com/${exp.url}?size=80` } alt={ `${exp.company} logo`}/>
                  </span>
                ) }

                <strong>{ exp.position }</strong> <br />{ exp.company }
              </h4>
              <small>{ exp.startDate } - { exp.endDate } <em>({ exp.duration })</em></small>
              <p>{ exp.description }</p>
              <ul>
                { exp.opportunities.map((item, i) => (
                  <li key={`opportunities-${i}`}>{ item }</li>
                ))}
              </ul>
              <hr />
            </div>
          )) }
      </div>

      <div className="container">
        <h2 className="title-section" id="#experiences">Testimonials</h2>
      </div>
      <div className="container article ">
          { cv.testimonials.map((exp, i) => (
            <div key={ `testimonial-${i}` } className="article-50">

              <blockquote>
                <p>{ exp.description }</p>
              </blockquote>
              <h5>
                <strong>{ exp.name }</strong><br /> { exp.position }
              </h5>
              <hr />
            </div>
          )) }
      </div>

      <Barcodes image="/uploads/norfolk.jpg" />
  </div>
))
