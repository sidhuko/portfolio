import { GenerateSW } from 'workbox-webpack-plugin'
import parser from './scripts/parser'
import Document from './src/Document'

function getPageByName(pages, name) {
  return pages.filter(n => (n.name === name))[0].data
}

export default {
  Document,
  siteRoot: "https://www.jamessidhu.com/",
  plugins: ['react-static-plugin-sass'],
  webpack: (config, { stage }) => {
    if (stage === 'prod') {
      config.entry = ['babel-polyfill', config.entry]
    } else if (stage === 'dev') {
      config.entry = ['babel-polyfill', ...config.entry]
    }

    config.plugins.push(new GenerateSW())

    return config
  },
  getSiteData: () => ({
    title: 'React Static with Netlify CMS',
  }),
  getRoutes: async() => {
    const posts = await parser('./src/content/posts')
    const pages = await parser('./src/content/pages')

    return [
      {
        path: '/',
        component: './src/pages/about',
        getData: () => ({ about: getPageByName(pages, 'about') }),
      },
      {
        path: '/cv',
        component: './src/pages/cv',
        getData: () => ({ cv: getPageByName(pages, 'cv') }),
      },
      {
        path: '/blog',
        component: 'src/pages/blog',
        getData: () => ({
          posts,
        }),
        children: posts.map(post => ({
          path: `/post/${post.slug}`,
          component: 'src/containers/Post',
          getData: () => ({
            post,
          }),
        })),
      },
      {
        path: '/contact',
        component: './src/pages/contact',
        getData: () => ({ contact: getPageByName(pages, 'contact') }),
      },
      {
        path: '*',
        component: './src/pages/404',
        is404: true,
      }
    ]
  },
}
